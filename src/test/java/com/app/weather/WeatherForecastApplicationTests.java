package com.app.weather;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.app.weather.domain.TempretureDetail;
import com.app.weather.service.WeatherService;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
//@WebMvcTest(value = WeatherForcastController.class)
class WeatherForecastApplicationTests {

	
	@Mock
	WeatherService weatherService;
	
	@Autowired
	private MockMvc mockMvc;
	
	TempretureDetail tempretureDetail = new TempretureDetail();
	
	@Test
    public void successCall() throws Exception {
		Mockito.when(
				weatherService.getTempreture(Mockito.anyString())).thenReturn(tempretureDetail);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/weather/85027").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());


		assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }
	
	@Test
    public void invalidZipTest() throws Exception {
		Mockito.when(
				weatherService.getTempreture(Mockito.anyString())).thenReturn(tempretureDetail);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/weather/850271").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getStatus());


		assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
    }
	
	@Test
    public void invalidZipMessageTest() throws Exception {
		Mockito.when(
				weatherService.getTempreture(Mockito.anyString())).thenReturn(tempretureDetail);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/weather/850271").accept(
				MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder)
				.andExpect(status().is4xxClientError())
			    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
			    .andExpect(jsonPath("$.message", is("Please enter Valid Zip Code")));


//		assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
    }
	
	
	

}
