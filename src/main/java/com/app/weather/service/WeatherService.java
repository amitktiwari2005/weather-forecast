package com.app.weather.service;

import org.springframework.stereotype.Service;

import com.app.weather.domain.TempretureDetail;
//This is interface and implemtation is provided in WeatherServiceImpl
@Service
public interface WeatherService {
	
	TempretureDetail getTempreture(String zipCode) throws Exception ;

}
