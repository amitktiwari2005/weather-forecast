package com.app.weather.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.weather.domain.TempretureDetail;
import com.app.weather.service.WeatherService;

/**
This class is responsible to get tomorrow Temperatures
This is controller Class, This URL need to use to invoke this class
http://localhost:8080/weather/85027
This is retunring tomorrow Temperature, Min and Max Temperature 
*/


@RestController
@CrossOrigin
@RequestMapping("/weather")  //http://localhost:8080/weather
public class WeatherForcastController {
	@Autowired
	WeatherService weatherService;
	
	
	
	
//	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(path = "/{zipCode}")
	public ResponseEntity<TempretureDetail> getTemperature(@PathVariable String zipCode) throws Exception {
		TempretureDetail tomorrowWeather = null;
		
		//Validation for Zip code
		if(zipCode.length() != 5) {
			throw new RuntimeException("Please enter Valid Zip Code");
		}
		tomorrowWeather = weatherService.getTempreture(zipCode);
		return new ResponseEntity<>(tomorrowWeather,HttpStatus.OK);

		
	}

}
