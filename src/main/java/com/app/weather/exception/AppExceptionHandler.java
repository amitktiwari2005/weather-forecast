package com.app.weather.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.app.weather.response.ErrorMessage;

/**
 * This class is responsible Throw exception in json format to Client
 */
@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	
	@ExceptionHandler(value = { RuntimeException.class })
	public ResponseEntity<ErrorMessage> handleRuntimeException(RuntimeException ex, WebRequest request) {

		String errorMessageDescription = ex.getLocalizedMessage();
		if (errorMessageDescription != null)
			errorMessageDescription.toString();

		ErrorMessage errorMessage = new ErrorMessage(new Date(), errorMessageDescription);
		return new ResponseEntity<ErrorMessage>(errorMessage, new HttpHeaders(), HttpStatus.BAD_REQUEST);

	}
	
	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<ErrorMessage> handleAnyException(Exception ex, WebRequest request) {

		String errorMessageDescription = ex.getLocalizedMessage();
		if (errorMessageDescription != null)
			errorMessageDescription.toString();

		ErrorMessage errorMessage = new ErrorMessage(new Date(), errorMessageDescription);
		return new ResponseEntity<ErrorMessage>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ExceptionHandler(value = { NullPointerException.class })
	public ResponseEntity<ErrorMessage> handleNullPointerException(NullPointerException ex, WebRequest request) {

		String errorMessageDescription = ex.getLocalizedMessage();
		if (errorMessageDescription != null)
			errorMessageDescription.toString();

		ErrorMessage errorMessage = new ErrorMessage(new Date(), errorMessageDescription);
		return new ResponseEntity<ErrorMessage>(errorMessage, new HttpHeaders(), HttpStatus.UNAUTHORIZED);

	}
	
	

}
