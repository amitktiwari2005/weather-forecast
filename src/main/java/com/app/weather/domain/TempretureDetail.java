package com.app.weather.domain;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class TempretureDetail {
	
	LocalDate date;
	double minTemp;
	double MaxTemp;
	LocalTime coolestTempTime;
	LocalTime hotestTempTime;
	List<HourlyTempreture> tempList;
	
	
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	public double getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(double minTemp) {
		this.minTemp = minTemp;
	}
	public double getMaxTemp() {
		return MaxTemp;
	}
	public void setMaxTemp(double maxTemp) {
		MaxTemp = maxTemp;
	}
	public List<HourlyTempreture> getTempList() {
		return tempList;
	}
	public void setTempList(List<HourlyTempreture> tempList) {
		this.tempList = tempList;
	}
	public LocalTime getCoolestTempTime() {
		return coolestTempTime;
	}
	public void setCoolestTempTime(LocalTime coolestTempTime) {
		this.coolestTempTime = coolestTempTime;
	}
	public LocalTime getHotestTempTime() {
		return hotestTempTime;
	}
	public void setHotestTempTime(LocalTime hotestTempTime) {
		this.hotestTempTime = hotestTempTime;
	}
	
	
}
