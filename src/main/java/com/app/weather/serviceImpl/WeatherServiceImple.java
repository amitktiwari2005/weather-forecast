package com.app.weather.serviceImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.app.weather.domain.HourlyTempreture;
import com.app.weather.domain.TempretureDetail;
import com.app.weather.service.WeatherService;

/**
 * Calling open Weather service where is is giving static data for next 4 days
 * and 3 hours. Even if you giving any zip code it will return same data. first
 * logic is written for to get response in json then parsing json and returning
 * json object which is having hashmap object
 */

@Service
public class WeatherServiceImple implements WeatherService {

	private final Logger logger = LoggerFactory.getLogger(WeatherServiceImple.class);
	DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@Override
	public TempretureDetail getTempreture(String zipCode) throws Exception {

		String httpURL = "https://samples.openweathermap.org/data/2.5/forecast/hourly?zip=" + zipCode
				+ "&appid=b6907d289e10d714a6e88b30761fae22";
		System.out.println("httpURL" + httpURL);
		try {
			URL url = new URL(httpURL);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			InputStreamReader inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String inputLine;
			StringBuilder data = new StringBuilder();
			while ((inputLine = bufferedReader.readLine()) != null) {
				data.append(inputLine).append("\n");
			}
			bufferedReader.close();
			inputStreamReader.close();
			urlConnection.disconnect();
			System.out.println("data.toString()" + data.toString());
			return callJsonExtractData(data.toString());
		} catch (Exception e) {
			logger.error("Internal server error", e);
			throw e;

		}
	}

	private TempretureDetail callJsonExtractData(String dataJSON) {

		final TempretureDetail tempretureDetail = new TempretureDetail();
		
		if (dataJSON != null) {
			try {
				tempretureDetail.setTempList(new ArrayList<HourlyTempreture>());
				JSONObject jsonRootArrayList = new JSONObject(dataJSON);
				JSONArray jsonRootArray = jsonRootArrayList.getJSONArray("list");
				double min_temp = 1000.0;
				double max_temp = -1000.0;
				LocalDateTime tomorrowdateTime = LocalDateTime.now().plusDays(1);
				for (Object item : jsonRootArray) {
					JSONObject tempretureObj = (JSONObject) item;
					String dateTimeStr = tempretureObj.getString("dt_txt");
					LocalDateTime dateTime = LocalDateTime.parse(dateTimeStr, dateTimeFormat);
					if (dateTime.toLocalDate().compareTo(LocalDate.parse("2019-03-27", dateFormat)) == 0) {
						
						HourlyTempreture hourlyTempreture = new HourlyTempreture();
						JSONObject mainObj = tempretureObj.getJSONObject("main");
						
						if(min_temp > mainObj.getDouble("temp_min")) {
							min_temp  = mainObj.getDouble("temp_min");
							tempretureDetail.setCoolestTempTime(dateTime.toLocalTime());
							
						}
						if(max_temp < mainObj.getDouble("temp_max")) {
							max_temp  = mainObj.getDouble("temp_max");
							tempretureDetail.setHotestTempTime(dateTime.toLocalTime());
						}
						hourlyTempreture.setTemp(mainObj.getDouble("temp"));
						hourlyTempreture.setTemp_min(mainObj.getDouble("temp_min"));
						hourlyTempreture.setTemp_max(mainObj.getDouble("temp_max"));
						hourlyTempreture.setPressure(mainObj.getDouble("pressure"));
						hourlyTempreture.setSea_level(mainObj.getDouble("sea_level"));
						hourlyTempreture.setGrnd_level(mainObj.getDouble("grnd_level"));
						hourlyTempreture.setHumidity(mainObj.getInt("humidity"));
						hourlyTempreture.setTime(dateTime.toLocalTime());
						
						tempretureDetail.getTempList().add(hourlyTempreture);
					} else {
						break;
					}
				}
				tempretureDetail.setDate(tomorrowdateTime.toLocalDate());
				tempretureDetail.setMaxTemp(max_temp);
				tempretureDetail.setMinTemp(min_temp);

			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		} else {
			logger.error("No data available for requested zip");
		}
		return tempretureDetail;
	}
}



